"use strict";

document.addEventListener("DOMContentLoaded", function(e) {

  let curPlayer = "X";
  const board = document.getElementById("gameboard");
  const changePlayer = document.getElementById("curplayer");
  const positions = [];
  let counter = 0;
  const winMessage = document.getElementsByTagName("aside")[0];
  
  function cellClicked(e){

    counter++;

    if(e.target.className === "xsquare" || e.target.className === "osquare"){
      return;
    }

    if(e.target.textContent === ""){
      e.target.textContent = curPlayer;
      
      if(e.target.textContent === "X"){
        e.target.classList.add("xsquare");
        curPlayer = "O";
        changePlayer.textContent = "O";

      }else if(e.target.textContent === "O"){
        e.target.classList.add("osquare");
        curPlayer = "X";
        changePlayer.textContent = "X";
      
      }
    }

    for(let i = 0; i < 9; i++){
      positions[i] = document.querySelectorAll("td")[i].textContent;
      if(document.querySelectorAll("td")[i].textContent === ""){
        positions[i] = false;
      }
    }
    
    let results = checkBoard(...positions);

    if(results === "X" || results === "O" || (results === false && counter === 9)){
        showWinningMessage(results);
        
    }
    
    if(winMessage.style.visibility === "visible"){
      return;
    }
    
  }
  
  board.addEventListener("click", cellClicked);


  function showWinningMessage(winner){
    const changeMessage = document.getElementsByTagName("h2")[1];

    if(winner === false){
      changeMessage.textContent = "No one won, it was a tie :^)";
    }else{
      changeMessage.textContent = winner + " won, the other player sucks";
    }
    debugger;
    winMessage.style.visibility = "visible";
  }

  const button = document.getElementById("reset");
  
  function newGame(e){

    for(let i = 0; i < positions.length; i++){
      const tds = document.querySelectorAll("td")[i];
      
      if(tds.textContent === "X"){
        tds.textContent = "";
        tds.classList.remove("xsquare");
      }else if(tds.textContent === "O"){
        tds.textContent = "";
        tds.classList.remove("osquare");
      }

    }
    counter = 0;
    winMessage.style.visibility = "hidden";
  }

  button.addEventListener("click", newGame);
  
})

// Put your DOMContentLoaded event listener here first.